Try to code up for problems in [LeetCode](https://leetcode.com/problemset/algorithms/).

The solution in this repo is implemented using [CoffeeScript](http://coffeescript.org), and [jsverify](https://github.com/jsverify/jsverify) is used
for property based testing. The `others` column contains solutions I steal from or I find neat.

