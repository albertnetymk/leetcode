fs = require 'fs'
db = require '../util/test_name'

p = console.log

others_data = fs.readFileSync './doc/others.txt', 'utf8'

others_json = {}

for line in others_data.split '\n'
  continue if line.length is 0
  columns = line.split ' '
  id = columns[0]
  id_int = parseInt id, 10
  continue if isNaN id_int
  url = columns[1]
  if columns.length <= 2
    comment = 'No comment.'
  else
    comment = columns[2..].join ' '
  others_json[id] = url: url, comment: comment

solution_ids = []

files = fs.readdirSync './solutions/'

for file in files
  chunks = file.split('.')
  continue unless chunks.pop() is 'coffee'
  id = chunks[0]
  solution_ids.push id

table_body = []
for id in solution_ids
  id_int = parseInt id, 10
  name = db.get_problem_name(id_int)
  solution = "[CoffeeScript](./solutions/#{id}.coffee)"
  other = others_json[id_int]
  if other
    others = "[#{other.comment}](#{other.url})"
  else
    others = '-'
  table_body.push [id_int, name, solution, others].join ' | '

fs.writeFileSync './doc/table_body.md', table_body.join '\n'
