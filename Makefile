test:
	mocha --reporter min --require coffeescript/register solutions/*.coffee

report:
	mocha --require coffeescript/register solutions/*.coffee

update-db:
	coffee ./util/get_problems.coffee

build:
	npm update && npm install

clean:
	rm -rf ./node_modules/

.PHONY: test update-db build clean
