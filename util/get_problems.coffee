request = require 'request'
fs = require 'fs'

p = console.log

# basically reprint from http://xyma.me/2016/12/16/Crawler-For-LeetCode/
opt =
  url: 'https://leetcode.com/api/problems/algorithms/'
  json: true

request opt, (err, res, body) ->
  items = body['stat_status_pairs']
  problems = {}
  for item in items
    id = item['stat']['question_id']
    title = item['stat']['question__title']
    problems[id] = title
  str = JSON.stringify problems, null, 2
  fs.writeFile './db/problems.json', str, (err) ->
    if err
      p 'ERROR: ', err
