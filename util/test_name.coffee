exports = this

path = require 'path'
fs = require 'fs'

problems = null

exports.get_problem_name = get_problem_name = (id) ->
  if problems is null
    problems = JSON.parse fs.readFileSync './db/problems.json', 'utf8'
  problems[id]

global.test_name = (filename) ->
  name = path.basename filename
  id = name.split('.')[0]
  id = parseInt id, 10
  id + '. ' + get_problem_name parseInt id, 10
