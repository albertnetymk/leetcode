require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

linear_search = (nums, target) ->
  [left, right] = [-1, -1]
  for v,i in nums
    if v is target
      left = i
      break

  for v,i in nums by -1
    if v is target
      right = i
      break
  [left, right]

# searchRange :: [Int] -> Int -> [Int]
searchRange = (nums, target) ->
  [left, right] = [-1, -1]

  [i, j] = [0, nums.length-1]
  while i < j
    mid = (i+j) >> 1
    if nums[mid] < target
      i = mid+1
      continue
    j = mid

  return [left, right] unless nums[i] is target

  left = i

  [i, j] = [0, nums.length-1]
  while i < j
    mid = ((i+j) >> 1) + 1
    if nums[mid] > target
      j = mid-1
      continue
    i = mid

  right = j

  [left, right]

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  repeat = (arr, i) ->
    return [i, arr] if i >= arr.length
    n = jsc.random(0, 10)
    [arr[i], arr[0...i].concat Array(n).fill(arr[i]), arr[i+1..]]

  input_pair_gen =
    jsc.array(jsc.integer)
      .smap(sort, _.identity)
      .smap(
        ((arr) -> repeat(arr, jsc.random(0, arr.length+1))),
        ((arr)->arr[1])
      )

  jsc.property 'searchRange naive algo', () ->
    nums = [5, 7, 7, 8, 8, 10]
    target = 8

    expect = linear_search nums, target
    actual = [3, 4]
    unless _.isEqual expect, actual
      p "expecting #{expect} but found #{actual}"
      return false
    true

  jsc.property 'searchRange naive == binary',
    input_pair_gen,
    (pair) ->
      [nums, target] = pair
      expect = linear_search nums, target
      actual = searchRange nums, target
      unless _.isEqual expect, actual
        p nums, target
        p "expecting #{expect} but found #{actual}"
        return false
      true
