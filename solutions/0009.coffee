require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

MAX = 2147483647
MIN = -2147483648

max_digits = do ->
  n = 0
  while true
    if Math.floor(MAX/Math.pow(10, n)) is 0
      return n
    n++

num_digits = (x) ->
  n = max_digits-1
  while n > 0
    if Math.floor(x/Math.pow(10, n)) isnt 0
      return n+1
    n--
  1

digit_at = (x, index) ->
  return x%10 if index is 0
  digit_at (Math.floor(x/10)), (index-1)

# isPalindrome :: Integer -> Boolean
isPalindrome = (x) ->
  return false if x < 0
  reverse_x = 0
  y = x
  while y > 9
    reverse_x = reverse_x * 10 + y%10
    y = Math.floor(y/10)
  reverse_x is Math.floor x/10

# isPalindrome = (x) ->
#   return false if x < 0
#   return false if x is MIN
#   x = Math.abs x
#   num = num_digits(x)
#   for n in [0..Math.floor(num/2)]
#     i = n
#     j = num - i - 1
#     digit_i = digit_at x, i
#     digit_j = digit_at x, j
#     return false if digit_i isnt digit_j
#   true

describe test_name(__filename), ->
  jsc.property 'isPalindrome', () ->
    cases = [
      [MIN, false]
      [1, true]
      [2, true]
      [10, false]
      [11, true]
      [-11, false]
      # [-11, true]
      [-12, false]
    ]
    for c in cases
      x = c[0]
      expect = c[1]
      actual = isPalindrome x
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true

  jsc.property 'max_digits', () ->
    max_digits is 10

  jsc.property 'num_digits', () ->
    cases = [
      [0, 1]
      [1, 1]
      [11, 2]
      [112, 3]
    ]
    for c in cases
      x = c[0]
      expect = c[1]
      actual = num_digits x
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true

  jsc.property 'digit_at', () ->
    cases = [
      [0, 0, 0]
      [1, 0, 1]
      [11, 0, 1]
      [11, 1, 1]
      [21, 1, 2]
    ]
    for c in cases
      x = c[0]
      index = c[1]
      expect = c[2]
      actual = digit_at x, index
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
