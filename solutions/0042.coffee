require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max

# trap :: [Int] -> Int
trap = (heights) ->
  result = altitude = 0
  [i, j] = [0, heights.length-1]
  while i < j
    if heights[i] <= heights[j]
      altitude = max altitude, heights[i]
      result += altitude - heights[i]
      i++
    else
      altitude = max altitude, heights[j]
      result += altitude - heights[j]
      j--
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'trap', () ->
    cases = [
      [
        [2,1,0,2],
        3
      ]
      [
        [4,2,0,3,2,5]
        9
      ]
      [
        [0,1,0,2,1,0,1,3,2,1,2,1]
        6
      ]
      [
        [6,4,2,0,3,2,0,3,1,4,5,3,2,7,5,3,0,1,2,1,3,4,6,8,1,3]
        83
      ]
    ]
    for c in cases
      input = c[0]
      expect = c[1]
      actual = trap input
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
