require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log


reverse_str = (str) ->
  str.split('').reverse().join ''

below_upper = (str) ->
  max = Math.pow(2,31) - 1
  max_str = max.toString()
  return false if str.length > max_str.length
  while str.length < max_str.length
    str = '0' + str
  str < max_str

reverse = (x) ->
  if x < 0
    x = -x
    prefix = '-'
  else
    prefix = ''

  x_str = x.toString()
  reversed = reverse_str x_str

  unless below_upper reversed, prefix
    return 0

  parseInt (prefix + reversed), 10

describe test_name(__filename), ->
  jsc.property 'special cases', () ->
    cases = [
      [ 123, 321 ]
      [ -123, -321 ]
      [ 10, 1 ]
      [ 100, 1 ]
      [ -100, -1 ]
      [ 1000000003, 0 ]
      [ -1000000003, 0 ]
    ]
    str = 'aba'
    for c in cases
      x = c[0]
      expect = c[1]
      actual = reverse x
      if actual isnt expect
        p "expect #{expect} but found #{actual}"
        return false
    true
