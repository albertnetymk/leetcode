require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# multiply :: String -> String -> String
multiply = (num1, num2) ->
  m = num1.length
  n = num2.length

  result = Array.apply(null, Array(m+n)).map(Number.prototype.valueOf,0)

  for c1,i in num1 by -1
    for c2,j in num2 by -1
      tmp = parseInt(c1) * parseInt(c2)
      [p1, p2] = [i+j, i+j+1]
      tmp += result[p1] * 10 + result[p2]

      result[p1] = Math.floor(tmp/10)
      result[p2] = tmp % 10

  first_non_zero_index = -1
  for e,i in result
    if e isnt 0
      first_non_zero_index = i
      break

  if first_non_zero_index is -1
    return "0"

  result[first_non_zero_index..].join ''

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'multiply', 'nat 1000000', 'nat 1000000', (x, y) ->
    num1 = x.toString()
    num2 = y.toString()

    expect = (x * y).toString()
    actual = multiply num1, num2

    unless expect is actual
      p "expecting #{expect} but found #{actual}"
      return false
    true
