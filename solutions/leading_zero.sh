#!/bin/zsh
for a in *.coffee; do
  b=$(printf %04d.coffee ${a%.coffee})
  if [ $a != $b ]; then
    mv $a $b
  fi
done
