require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

limit5 = (str) ->
  index = str.indexOf '.'
  str[..index+5]

myPow = (x, n) ->
  result = Math.pow(x, n)
  result += 0.000005
  result_str = result.toString()
  result_str = limit5 result_str
  parseFloat result_str

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'myPow', () ->
    cases =
      [
        [2, 10, 1024]
        [2.1, 3, 9.261]
        [8.88023, 3, 700.28148]
        [34.00515, -3, 0.00003]
      ]
    for c in cases
      [x,n] = c[0..1]
      expect = c[2]
      actual = myPow x, n
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
