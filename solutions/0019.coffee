require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# removeNthFromEnd :: Node -> Int -> Node
removeNthFromEnd = (head, n) ->
  dummy =
    next: head
  first = second = dummy
  for i in [1..n+1] by 1
    first = first.next
  while first
    first = first.next
    second = second.next
  second.next = second.next.next
  dummy.next

# # using auxiliary array
# removeNthFromEnd = (head, n) ->
#   arr = []
#   while true
#     arr.push head
#     head = head.next
#     break if head is null
#   len = arr.length
#   arr.push null
#   if n is len
#     arr[1]
#   else
#     arr[len-n-1].next = arr[len-n+1]
#     arr[0]

describe test_name(__filename), ->
  class Node
    constructor: (@val, @next) ->

  build_from_list = (list) ->
    arr = (new Node(e, null) for e in list)
    for i in [1..arr.length-1]
      arr[i-1].next = arr[i]
    arr[0]
  show = (x) -> p JSON.stringify x

  jsc.property 'removeNthFromEnd', () ->

    list =  build_from_list [1..5]
    ans = build_from_list [1,2,3,5]
    my = removeNthFromEnd(list, 2)
    _.isEqual ans, my
