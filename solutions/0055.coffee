require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)


# canJump :: [Integer] -> Boolean
canJump = (nums) ->
  target = nums.length - 1
  i = far = 0
  while i <= far
    far = max far, i + nums[i]
    return true if far >= target
    i++
  false

# I thought plain bfs could work...
naive_bfs = (nums) ->
  target = nums.length-1
  visited = {}

  # step :: [Index] -> [Index]
  step = (srcs) ->
    return false if srcs.length is 0
    return true if srcs.indexOf(target) isnt -1
    dict = {}
    for s in srcs
      visited[s] = true
      for i in outs s
        dict[i] = true
    step (parseInt(k) for k in Object.keys dict when visited[k] is undefined)

  # outs :: Index -> [Index]
  outs = (index) ->
    return [] if nums[index] is 0
    low = max 0, index-nums[index]
    high = min nums.length-1, index+nums[index]
    i for i in [low..high] by 1 when i isnt index

  step [0]

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'canJump', () ->
    cases = [
      [true, [2,3,1,1,4]]
      [false, [3,2,1,0,4]]
    ]

    for c in cases
      expect = c[0]
      actual = canJump c[1]
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        p c
        return false
    true
