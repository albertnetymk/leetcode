require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# reverseKGroup :: Node -> Int -> Node
reverseKGroup = (head, k) ->
  tmp = head
  for i in [1..k] by 1
    return head if tmp is null
    tmp = tmp.next

  pre = reverseKGroup tmp, k

  cur = head
  for i in [1..k] by 1
    tmp = cur.next
    cur.next = pre
    pre = cur
    cur = tmp

  pre

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true
  class Node
    constructor: (@val, @next) ->
  build_from_list = (list) ->
    arr = (new Node(e, null) for e in list)
    for i in [1..arr.length-1] by 1
      arr[i-1].next = arr[i]
    arr[0]
  jsc.property 'reverseKGroup', () ->
    l1 =  build_from_list [1..5]
    ans_2 =  build_from_list [2,1,4,3,5]
    my_2 = reverseKGroup l1, 2

    l1 =  build_from_list [1..5]
    ans_3 =  build_from_list [3,2,1,4,5]
    my_3 = reverseKGroup l1, 3
    all [
      _.isEqual ans_2, my_2
      _.isEqual ans_3, my_3
    ]
