require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# strStr ::
strStr = (haystack, needle) ->
  return 0 if needle is ''
  l = needle.length

  for i in [0..haystack.length-1] by 1
    if haystack[i..i+l-1] is needle
      return i
  return -1
  # haystack.indexOf needle

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'strStr', () ->
    all [
      0 == strStr "hello", ""
      0 == strStr "hello", "he"
      -1 == strStr "ello", "he"
      6 == strStr "hello world", "wo"
    ]
