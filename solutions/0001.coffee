require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

twoSum = (nums, target) ->
  dict = {}
  for x,i in nums
    y = target-x
    if y of dict
      return [dict[y]+1, i+1]
    dict[x] = i

unique_array = jsc.nearray(jsc.nat).smap(_.uniq, _.identity)

describe test_name(__filename), ->
  jsc.property '', unique_array, (arr) ->
    l = arr.length
    return true if l is 0
    i = jsc.random 0, l-1
    j = jsc.random 0, l-1
    return true if i == j
    target = arr[i] + arr[j]
    [i, j] = twoSum arr, target
    i<=j and arr[i-1] + arr[j-1] == target
