require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

div = (x, y) -> Math.floor(x / y)

b_1000 = ["", "M", "MM", "MMM"]
b_100 = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
b_10 = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
b_1 = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]

# intToRoman :: Int -> String
intToRoman = (x) ->

  b_1000[div x, 1000] +
  b_100[div (x%1000), 100] +
  b_10[div (x%100), 10] +
  b_1[div (x%10), 1] +
  ""

describe test_name(__filename), ->
  jsc.property 'intToRoman', () ->
    'MMMCMXCIX' == intToRoman 3999
