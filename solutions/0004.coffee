require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

single_array_median = (arr) ->
  l = arr.length
  return NaN if l is 0
  positions = l * 2 + 1
  middle = positions >> 1
  (arr[(middle-1)>>1] + arr[middle>>1])/2

MAX = Number.MAX_VALUE

findMedianSortedArrays = (nums1, nums2) ->
  if nums1.length > nums2.length
    [nums1, nums2] = [nums2, nums1]
  # nums1 is smaller
  n1 = nums1.length
  n2 = nums2.length
  return single_array_median(nums2) if n1 is 0
  total = n1 + n2
  start = 0
  end = n1 * 2 + 1
  while start < end
    c1 = (start+end) >> 1
    c2 = total - c1
    l1 = if c1 is 0 then -MAX else nums1[(c1-1)>>1]
    r1 = if c1>>1 >= n1 then MAX else nums1[c1>>1]
    l2 = if c2 is 0 then -MAX else nums2[(c2-1)>>1]
    r2 = if c2>>1 >= n2 then MAX else nums2[c2>>1]
    if l1 > r2
      end = c1
    else if l2 > r1
      start = c1+1
    else
      return (Math.max(l1, l2) + Math.min(r1, r2))/2

  return NaN

naive_median = (nums1, nums2) ->
  combined = nums1.concat nums2
  combined.sort((a,b)-> a-b)
  single_array_median combined

_.sort = (arr) -> _.sortBy(arr, (x)->(x))
sorted_array = jsc.array(jsc.nat).smap(_.sort, _.uniq, _.identity)

describe test_name(__filename), ->
  jsc.property '', sorted_array, sorted_array, (arr1, arr2) ->
    m1 = naive_median arr1, arr2
    m2 = findMedianSortedArrays arr1, arr2
    both_nan = isNaN(m1) and isNaN(m2)
    eq = m1 is m2
    both_nan or eq
