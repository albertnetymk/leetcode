require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# isValidSudoku :: [[Char]]
isValidSudoku = (board) ->
  used_row = Array(9).fill().map(-> Array(9).fill(false))
  used_col = Array(9).fill().map(-> Array(9).fill(false))
  used_box = Array(9).fill().map(-> Array(9).fill(false))

  for i in [0...9]
    for j in [0...9]
      if board[i][j] is '.'
        continue
      num = parseInt(board[i][j]) - 1
      k = Math.floor(i/3) * 3 + Math.floor(j/3)
      return false if used_row[i][num] or used_col[j][num] or used_box[k][num]
      used_row[i][num] = used_col[j][num] = used_box[k][num] = true

  true

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'isValidSudoku', () ->
    cases = [
      [
        [".87654321"
          "2........"
          "3........"
          "4........"
          "5........"
          "6........"
          "7........"
          "8........"
          "9........"]
        true
      ]
    ]
    for c in cases
      actual = isValidSudoku c[0]
      expect = c[1]
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
