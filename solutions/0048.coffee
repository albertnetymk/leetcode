require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

exchange = (m, x, y, value) ->
  tmp = m[x][y]
  m[x][y] = value
  tmp

# transform: [i, j] -> [j, l - i]
rotate4 = (m, i, j, l) ->
  tmp = m[i][j]
  for count in [1..4]
    tmp = exchange m, j, (l-i), tmp
    [i, j] = [j, l-i]

# rotate :: [[Int]] -> void
rotate = (m) ->
  l = m.length
  row_half = l >> 1
  for i in [0...row_half] by 1
    for j in [i...l-i-1] by 1
      rotate4 m, i, j, (l-1)
  return

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'rotate', () ->
    result = []

    m =
      [
        [1,2,3]
        [4,5,6]
        [7,8,9]
      ]
    rotate m
    expect =
      [
        [7,4,1]
        [8,5,2]
        [9,6,3]
      ]
    result.push _.isEqual m, expect
    m =
      [
        [ 5, 1, 9,11]
        [ 2, 4, 8,10]
        [13, 3, 6, 7]
        [15,14,12,16]
      ]
    rotate m
    expect =
      [
        [15,13, 2, 5]
        [14, 3, 4, 1]
        [12, 6, 8, 9]
        [16, 7,10,11]
      ]
    result.push _.isEqual m, expect

    all result
