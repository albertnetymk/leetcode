require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

filter = (lists) -> n for n in lists when n isnt null

sorted_index = (arr, value) ->
  [low, high] = [0, arr.length]
  while low < high
    mid = (low + high) >>> 1
    if arr[mid].val < value
      low = mid + 1
    else
      high = mid
  low
 
# mergeKLists :: [Node] -> Node
mergeKLists = (lists) ->
  dummy = next: null 
  first = dummy
  lists = filter lists
  lists.sort (n1, n2) -> n1.val - n2.val
  while lists.length > 0
    first = first.next = lists.shift()
    next = first.next
    if next
      i = sorted_index lists, next.val
      lists.splice i, 0, next

  dummy.next

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true
  class Node
    constructor: (@val, @next) ->
  build_from_list = (list) ->
    arr = (new Node(e, null) for e in list)
    for i in [1..arr.length-1] by 1
      arr[i-1].next = arr[i]
    arr[0]

  jsc.property 'mergeKLists', () ->
    l0 =  build_from_list [0]
    l1 =  build_from_list [1,3,5,7]
    l2 =  build_from_list [2,4,6]
    ans = build_from_list [0..7]
    my = mergeKLists([l0, l1, l2])
    _.isEqual ans, my
