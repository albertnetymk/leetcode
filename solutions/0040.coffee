require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# combinationSum2 :: [Int] -> Int -> [[Int]]
combinationSum2 = (candidates, target) ->
  candidates = sort candidates
  result = []
  f = (j, history, quota) ->
    if quota is 0
      result.push history
      return
    while j>= 0
      if candidates[j] <= quota
        cur = candidates[j]
        f j-1, history.concat([cur]), quota-cur
        while j > 0 and candidates[j-1] is candidates[j]
          j--
      j--
    return
  f candidates.length-1, [], target
  result


describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'combinationSum2', () ->
    candidates = [10, 1, 2, 7, 6, 1, 5]
    target = 8
    expect = [
      [1, 7]
      [1, 2, 5]
      [2, 6]
      [1, 1, 6]
    ]
    actual = combinationSum2 candidates, target
    expect = expect.map(sort)
    actual = actual.map(sort)
    expect.sort()
    actual.sort()
    return false unless expect.length is actual.length
    for i in [0...expect.length]
      return false unless _.isEqual expect[i], actual[i]
    true
