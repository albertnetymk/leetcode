require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# distributeCandies :: [Int] -> Int
distributeCandies = (candies) ->
  dict = {}
  for c in candies
    c = c.toString()
    if dict[c] is undefined
      dict[c] = true

  min (candies.length >> 1), Object.keys(dict).length

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'distributeCandies', () ->
    cases = [
      [[1, 1, 2, 2, 3, 3], 3]
      [[1, 1, 2, 3], 2]
    ]

    for c in cases
      [input, expect] = c
      actual = distributeCandies input
      unless expect is actual
        p input
        p "expecting #{expect} but found #{actual}"
        return false
    true
