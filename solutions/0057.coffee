require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

class Interval
  constructor: (@start, @end) ->

# find the interval with the largest start that's smaller than or equal key
# binary_search :: [Interval] -> Int -> Int -> Int -> Int
binary_search = (arr, start, end, key) ->
  return -1 if key < arr[start].start
  return end if key >= arr[end].start
  # +1 so that middle is rounded up;
  # otherwise infinite loop when [start, end] == [middle, end]
  middle = (start+end+1) >> 1
  if key < arr[middle].start
    return binary_search arr, start, middle-1, key
  else if key > arr[middle].start
    return binary_search arr, middle, end, key
  else
    return middle

# insert :: [Interval] -> Interval -> [Interval]
insert = (intervals, newInterval) ->
  return [newInterval] if intervals.length is 0
  index = binary_search intervals, 0, intervals.length-1, newInterval.start
  if index is -1
    result = [newInterval]
  else
    result = intervals[0..index]
    previous = result[result.length-1]
    if newInterval.start <= previous.end
      previous.end = max previous.end, newInterval.end
    else
      result.push newInterval
  for i in [index+1...intervals.length] by 1
    previous = result[result.length-1]
    current = intervals[i]
    if current.start > previous.end
      return result.concat(intervals[i..])
    previous.end = max previous.end, current.end
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'merge', () ->
    cases =
      [
        [[], [5,7], [[5,7]]]
        [[[1,3],[6,9]], [2,5], [[1,5],[6,9]]]
        [[[1,2],[3,5],[6,7],[8,10],[12,16]], [4,9], [[1,2],[3,10],[12,16]]]
        [[[1,5]], [6,8], [[1,5],[6,8]]]
      ]
    for c in cases
      intervals = (new Interval e[0], e[1] for e in c[0])
      newInterval = new Interval c[1][0], c[1][1]
      expect = c[2]
      actual = insert intervals, newInterval
      actual = ([i.start, i.end] for i in actual)
      if not _.isEqual actual, expect
        p "expecting #{expect} but found #{actual}"
        return false
    true
