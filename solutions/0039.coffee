require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# combinationSum :: [Int] -> Int -> [[Int]]
combinationSum = (candidates, target) ->
  candidates = sort candidates
  result = []
  f = (j, history, quota) ->
    if quota is 0
      result.push history
      return
    while j>= 0
      if candidates[j] <= quota
        cur = candidates[j]
        f j, history.concat([cur]), quota-cur
      j--
    return
  f candidates.length-1, [], target
  result


describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'combinationSum', () ->
    candidates = [2, 3, 6, 7]
    target = 7
    expect = [
      [7],
      [2, 2, 3]
    ]
    actual = combinationSum candidates, target
    expect = expect.map(sort)
    actual = actual.map(sort)
    expect.sort()
    actual.sort()
    return false unless expect.length is actual.length
    for i in [0...expect.length]
      return false unless _.isEqual expect[i], actual[i]
    true
