require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# type Board = [Int]

# board_to_str :: Board -> [String]
board_to_str = (b) ->
  n = b.length
  # empty board
  matrix = new Array(n)
  for i in [0...n] by 1
    matrix[i] = new Array(n)
    for j in [0...n]
      matrix[i][j] = '.'

  for index,i in b
    matrix[index][i] = 'Q'

  for r,i in matrix
    matrix[i] = r.join ''

  matrix

# invalid :: Board -> Int -> Bool
invalid = (board, new_c) ->
  new_c_i = board.length
  for c,c_i in board
    return true if abs(c - new_c) is (new_c_i - c_i)
  false

# queens_bfs :: Int -> [Board]
queens_bfs = (n) ->
  boards = [[]]
  for depth in [0...n]
    new_boards = []
    for b,index in boards
      level = []
      for i in [0...n]
        continue if i in b or invalid b, i
        new_b = b[..]
        new_b.push i
        level.push new_b
      new_boards = new_boards.concat level
    boards = new_boards
  boards

new_bool_array = (size) ->
  ret = new Array(size)
  for i in [0...size] by 1
    ret[i] = false
  ret

# queens_dfs :: Int -> [Board]
queens_dfs = (n) ->
  cols = new_bool_array n
  # diagnal / x - y = const
  d1 = new_bool_array (n*2)
  # diagnal \ x + y = const
  d2 = new_bool_array (n*2)
  boards = []

  dfs = (row, board) ->
    if row is n
      boards.push board
      return
    for c in [0...n]
      d1_index = c - row + n
      d2_index = c + row
      continue if cols[c] or d1[d1_index] or d2[d2_index]
      cols[c] = d1[d1_index] = d2[d2_index] = true
      dfs row+1, board[..].concat([c])
      cols[c] = d1[d1_index] = d2[d2_index] = false
  dfs 0, []

  boards

# solveNQueens :: Int -> [[String]]
solveNQueens = (n) ->
  board_to_str b for b in queens_dfs n
  # board_to_str b for b in queens_bfs n

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'solveNQueens', () ->
    expect =
      [
        [".Q.."
          "...Q"
          "Q..."
          "..Q."]

        ["..Q."
          "Q..."
          "...Q"
          ".Q.."]
      ]
    actual = solveNQueens 4
    expect.sort()
    actual.sort()
    _.isEqual expect, actual
