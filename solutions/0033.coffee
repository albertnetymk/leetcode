require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

search_linear = (nums, target) ->
  for v,i in nums
    return i if v is target
  return -1

find_smallest_index = (arr) ->
  min = arr[arr.length-1]
  low = 0
  high = arr.length-1
  while low < high
    mid = (low+high) >> 1
    if arr[mid] < min
      min = arr[mid]
      high = mid
    else
      low = mid + 1
  low

binary_search_index = (arr, low, high, target) ->
  while low < high
    mid = (low+high) >> 1
    if target is arr[mid]
      return mid
    if target < arr[mid]
      high = mid
      continue
    low = mid+1
  -1

search = (nums, target) ->
  return -1 if nums.length is 0
  i = find_smallest_index nums
  if target > nums[nums.length-1]
    return binary_search_index nums, 0, i, target
  binary_search_index nums, i, nums.length, target

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  rotate = (arr, i) ->
    return arr if i is 0 or isNaN i
    arr[i..arr.length-1].concat arr[0..i-1]

  uniq_sorted_int_array_gen =
    jsc.array(jsc.integer)
      .smap(_.uniq, _.identity)
      .smap(sort, _.identity)
      .smap(((arr)->rotate(arr, jsc.random(0, arr.length-1))), _.identity)
  int_gen = jsc.integer

  jsc.property 'search',
    uniq_sorted_int_array_gen,
    int_gen, (nums, target, i) ->
      expect = search_linear nums, target
      actual = search nums, target
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
      true
