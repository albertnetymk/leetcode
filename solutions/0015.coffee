require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

# threeSum :: [Int] -> [[Int]]
threeSum = (nums) ->
  nums = nums.sort() # not numeric sort, but ok as well
  dict = {}
  for i in [0..nums.length-1] by 1
    n = nums[i]
    if dict[n]
      dict[n].push i
    else
      dict[n] = [i]

  ret = {}
  for i in [0..nums.length-1] by 1
    a = nums[i]
    for j in [i+1..nums.length-1] by 1
      b = nums[j]
      target = 0 - a - b
      indices = dict[target]
      if indices and indices[indices.length-1] > j
        ret[[a,b]] = [a, b, target]

  v for k,v of ret

describe test_name(__filename), ->
  jsc.property 'threeSum', () ->
    eq = (x, y) ->
      _.isEqual x.sort(), y.sort()
    ret = threeSum [-1, 0, 1, 2, -1, -4]
    eq ret, [ [ -1, 0, 1 ], [ -1, -1, 2 ] ]
