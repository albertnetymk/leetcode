require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# firstMissingPositive :: [Int] -> Int
firstMissingPositive = (nums) ->
  swap = (i, j) ->
    tmp = nums[i]
    nums[i] = nums[j]
    nums[j] = tmp
  l = nums.length
  for i in [0...l]
    while nums[i] > 0 and nums[i] < l and nums[nums[i]-1] isnt nums[i]
      swap i, nums[i]-1
  for i in [0...l]
    if nums[i] isnt i+1
      return i+1
  l+1

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'firstMissingPositive', () ->
    cases = [
      [
        [1, 2, 0], 3
      ]
      [
        [3, 4, -1, 1], 2
      ]
    ]
    for c in cases
      nums = c[0]
      expect = c[1]
      actual = firstMissingPositive nums
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
