require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max

Array.prototype.top = -> this[this.length-1]
Array.prototype.top_or = (x) ->
  if this.length > 0
    this.top()
  else
    x

# longestValidParentheses :: String -> Int
longestValidParentheses = (s) ->
  length = s.length
  stack = []
  longest = i = 0
  while i < length
    switch
      when s[i] is ')' and stack.length > 0 and s[stack.top()] is '('
        stack.pop()
        longest = max longest, i - stack.top_or -1
      else
        stack.push i
    i++
  longest

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'longestValidParentheses', () ->
    cases = [
      ['()', 2]
      ['(()', 2]
      [')()())', 4]
      ['()(()', 2]
      [')(((((()())()()))()(()))(', 22]
    ]
    for c in cases
      expect = c[1]
      actual = longestValidParentheses c[0]
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
