require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

int_min = -2147483648
int_max = 2147483647

# divide :: Int -> Int -> Int
divide = (dividend, divisor) ->
  return int_max if dividend is int_min and divisor is -1
  return dividend if divisor is 1
  return -dividend if divisor is -1
  sign = (dividend > 0 and divisor > 0) or (dividend < 0 and divisor < 0)
  dividend = abs dividend
  divisor = abs divisor
  n = 0
  while dividend >= divisor
    i = 0
    tmp = divisor
    while true
      if dividend == tmp
        dividend = 0
        n += 1 << i
        break
      if dividend < tmp
        dividend -= tmp >>> 1
        n += 1 << (i-1)
        break
      tmp = (tmp << 1) >>> 0
      i++
  if sign then n else -n

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'divide', () ->
    all [
      int_max == divide int_min, -1
      1 == divide 1, 1
      -1 == divide 1, -1
      0 == divide 1, 2
      1 == divide int_min, int_min
      int_min == divide int_min, 1
      int_max == divide int_max, 1
      ((1 << 30) - 1) == divide (int_max-1)/2, 1
      ((1 << 29) - 1) == divide (int_max-1)/2, 2
    ]

  jsc.property 'random', () ->
    jsc.forall 'int32', 'int32', (x, y) ->
      if x is int_min and y is -1
        true
      else
        (x/y | 0) == divide x, y
