require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

next = (str) ->
  ret = []
  i = 0
  while true
    c = str[i]
    j = i+1
    while j < str.length and str[j] is c
      j++
    ret.push j-i
    ret.push c
    if j is str.length
      break
    i = j
  ret.join ''

# countAndSay :: Int -> String
countAndSay = (n) ->
  i = 1
  str = "1"
  while i < n
    str = next str
    i++
  str

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'countAndSay', () ->
    seq = [
      1
      11
      21
      1211
      111221
    ]
    for i in [0...seq.length]
      expect = seq[i].toString()
      actual = countAndSay i+1
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
