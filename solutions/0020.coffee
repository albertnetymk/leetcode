require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# isValid :: String -> Bool
isValid = (s) ->
  dict =
    '(': ')'
    '[': ']'
    '{': '}'
  stack = []
  for c in s
    if dict[c]
      stack.push c
    else
      unless dict[stack.pop()] is c
        return false
  stack.length is 0

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'isValid', () ->
    all [
      not isValid '('
      isValid '()'
      isValid '[]'
      isValid '[{}]'
      not isValid '[(}]'
    ]
