require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# generateParenthesis :: Int -> [String]
generateParenthesis = (n) ->
  return [''] if n is 0
  ret = []
  for i in [0..n-1] by 1
    for l in generateParenthesis i
      for r in generateParenthesis(n-i-1)
        ret.push "(#{l})#{r}"
  ret

describe test_name(__filename), ->
  eq = (a1, a2) -> _.isEqual a1.sort(), a2.sort()

  jsc.property 'generateParenthesis', () ->
    ans =  [
      "(((())))"
      "((()()))"
      "((())())"
      "((()))()"
      "(()(()))"
      "(()()())"
      "(()())()"
      "(())(())"
      "(())()()"
      "()((()))"
      "()(()())"
      "()(())()"
      "()()(())"
      "()()()()"
    ]
    my = generateParenthesis 4
    eq ans, my
