require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# searchInsert :: [Int] -> Int -> Int
searchInsert = (nums, target) ->
  low = 0
  high = nums.length
  while low < high
    mid = (low+high) >> 1
    if nums[mid] is target
      return mid
    if target < nums[mid]
      high = mid
      continue
    low = mid+1
  low

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'searchInsert', () ->
    cases = [
      [[1], 0, 0]
      [[1,3,5,6], 5, 2]
      [[1,3,5,6], 2, 1]
      [[1,3,5,6], 7, 4]
      [[1,3,5,6], 0, 0]
    ]
    for c in cases
      expect = c[2]
      actual = searchInsert c[0], c[1]
      unless expect is actual
        p c
        p "expecting #{expect} but found #{actual}"
        return false
    true
