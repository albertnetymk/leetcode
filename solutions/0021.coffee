require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# mergeTwoLists :: Node -> Node -> Node
mergeTwoLists = (l1, l2) ->
  h = l = next: null
  while l1 && l2
    if l1.val < l2.val
      l.next = l1
      l1 = l1.next
    else
      l.next = l2
      l2 = l2.next
    l = l.next
  if l1
    l.next = l1
  else
    l.next = l2

  h.next

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true
  class Node
    constructor: (@val, @next) ->

  build_from_list = (list) ->
    arr = (new Node(e, null) for e in list)
    for i in [1..arr.length-1]
      arr[i-1].next = arr[i]
    arr[0]

  show = (x) -> p JSON.stringify x

  jsc.property 'mergeTwoLists', () ->
    l1 =  build_from_list [1,3,5]
    l2 =  build_from_list [2,4,6]
    ans = build_from_list [1..6]
    my = mergeTwoLists(l1, l2)
    _.isEqual ans, my
