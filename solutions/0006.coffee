require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

# get_pair_list :: Int -> Int -> [[Int, Int]]
get_pair_list = (str_len, row) ->
  result = []
  j = 0
  v_block_size = 2*row - 2
  v_block_num = Math.ceil str_len / v_block_size
  for j in [0..row*v_block_num]
    switch j % (row-1)
      when 0
        for i in [0..row-1]
          result.push [i, j]
      else
        result.push [row-1 - j%(row-1), j]
    return result[0...str_len] if result.length >= str_len
  result[0...str_len]

construct_map_from_pair_list = (list, row) ->
  map = {}
  for i in [0..row-1]
    map[i] = { counter: 0 }
  for pair in list
    [i, j] = pair
    map[i][j] = map[i].counter++
  pre_size = 0
  for i,line of map
    line.pre_size = pre_size
    pre_size += line.counter
  map

get_index = (i, j, map) ->
  map[i][j] + map[i].pre_size

# elaborate_convert :: String -> Int -> String
elaborate_convert = (s, numRows) ->
  str_len = s.length
  return s if numRows is 1
  result = []
  row = numRows
  list = get_pair_list str_len, row
  map = construct_map_from_pair_list list, row
  for pair,key in list
    [i, j] = pair
    index = get_index i, j, map
    result[index] = s[key]
  result.join ''

convert = (s, numRows) ->
  result = []
  row = numRows
  return s if row is 1
  cycle = 2*row - 2
  v_block_num = Math.floor s.length / cycle
  for i in [0..row-1]
    j = i
    while j < s.length
      result.push s[j]
      if i isnt 0 and i isnt row-1
        second_index = j + (row-i-1) + (row-i-2) + 1
        result.push s[second_index] if second_index < s.length
      j += cycle

  result.join ''

unique_array = jsc.nearray(jsc.nat).smap(_.uniq, _.identity)

describe test_name(__filename), ->
  jsc.property 'particular cases for convert', () ->
    cases = [
      ['abcde', 4, 'abced']
      ['PAYPALISHIRING', 3, 'PAHNAPLSIIGYIR']
    ]

    for c in cases
      input = c[0]
      row = c[1]
      expect = c[2]
      actual = convert input, row
      if actual isnt expect
        p "expect #{expect}, but found #{actual}"
        return false
    true
  jsc.property 'particular cases for elaborate_convert', () ->
    cases = [
      ['abcde', 4, 'abced']
      ['PAYPALISHIRING', 3, 'PAHNAPLSIIGYIR']
    ]

    for c in cases
      input = c[0]
      row = c[1]
      expect = c[2]
      actual = elaborate_convert input, row
      if actual isnt expect
        p "expect #{expect}, but found #{actual}"
        return false
    true
  jsc.property 'get_pair_list', () ->
    cases = [
      [5, 3,
        [ [ 0, 0 ], [ 1, 0 ], [ 2, 0 ], [ 1, 1 ], [ 0, 2 ] ] ]
      [6, 3,
        [ [ 0, 0 ], [ 1, 0 ], [ 2, 0 ], [ 1, 1 ], [ 0, 2 ], [ 1, 2 ] ] ]
    ]
    for c in cases
      str_len = c[0]
      row = c[1]
      expect = c[2]
      actual = get_pair_list str_len, row
      unless _.isEqual actual, expect
        p actual
        p expect
        return false
    true
  jsc.property 'construct_map_from_pair_list', () ->
    expect =
      '0': { '0': 0, '2': 1, counter: 2, pre_size: 0 }
      '1': { '0': 0, '1': 1, counter: 2, pre_size: 2 }
      '2': { '0': 0, counter: 1, pre_size: 4 }

    actual = construct_map_from_pair_list (get_pair_list 5, 3), 3
    unless _.isEqual actual, expect
      p actual
      p expect
      return false
    true
  jsc.property 'get_index_from_pair', () ->
    map = construct_map_from_pair_list (get_pair_list 5, 3), 3
    cases = [
      [[0, 0], 0]
      [[1, 0], 2]
      [[2, 0], 4]
      [[1, 1], 3]
      [[0, 2], 1]
    ]
    for c in cases
      i = c[0][0]
      j = c[0][1]
      expect = c[1]
      actual = get_index i, j, map
      if actual isnt expect
        p "expect #{expect}, but found #{actual}"
        return false
    true
