require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

dict =
  M: 1000
  D: 500
  C: 100
  L: 50
  X: 10
  V: 5
  I: 1

# romanToInt :: String -> Int
romanToInt = (str) ->
  last = str.length-1
  ret = dict[str[last]]
  for i in [last-1..0] by -1
    if dict[str[i]] < dict[str[i+1]]
      ret -= dict[str[i]]
    else
      ret += dict[str[i]]
  ret

describe test_name(__filename), ->
  jsc.property 'romanToInt', () ->
    3999 == romanToInt 'MMMCMXCIX'
