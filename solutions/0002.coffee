require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

ListNode = (v) ->
  this.val = v
  this.next = null

list_eq = (l1, l2) ->
  return true if l1 is null and l2 is null
  return false if l1 is null or l2 is null
  l1.val is l2.val and list_eq l1.next, l2.next

addTwoNumbers = (l1, l2) ->
  carrier = 0
  head = pre = new ListNode 0
  while l1 isnt null or l2 isnt null or carrier isnt 0
    if l1 isnt null
      carrier += l1.val
      l1 = l1.next
    if l2 isnt null
      carrier += l2.val
      l2 = l2.next
    pre.next = new ListNode carrier%10
    carrier = Math.floor carrier/10
    pre = pre.next

  head.next

unique_array = jsc.nearray(jsc.nat).smap(_.uniq, _.identity)

str_to_list = (str) ->
  return null if str.length is 0
  ret = {}
  ret.val = parseInt str[0]
  ret.next = str_to_list str[1..]
  ret

describe test_name(__filename), ->
  jsc.property 'particular cases', () ->
    cases = [
      ['', '', '']
      ['0', '0', '0']
      ['1', '0', '1']
      ['1', '9', '01']
      ['10', '9', '01']
      ['101', '9', '011']
    ]

    for c in cases
      l1 = str_to_list c[0]
      l2 = str_to_list c[1]
      expect = str_to_list c[2]
      ret = addTwoNumbers l1, l2
      unless list_eq ret, expect
        p l1
        p l2
        p ret
        p expect
        return false
    true

  describe 'list_eq', ->
    jsc.property 'identity eq', () ->
      l1 =
        val: 0
        next: null
      list_eq l1, l1

    jsc.property 'particular cases', () ->
      l1 =
        val: 0
        next: null
      l2 =
        val: 0
        next: null
      list_eq l1, l2

    jsc.property 'particular cases', () ->
      l1 =
        val: 0
        next: null
      l2 =
        val: 1
        next: null
      false is list_eq l1, l2
