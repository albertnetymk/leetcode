require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

permuteUnique = (nums) ->
  result = [[]]
  for x in nums
    new_result = []
    for arr in result
      for i in [0..arr.length] by 1
        new_result.push arr[...i].concat [x], arr[i..]
        # break on first occurrence of x
        break if i < arr.length and arr[i] is x
    result = new_result
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'permuteUnique', () ->
    mine = permuteUnique [1,1,2]
    expect =
      [
        [2,1,1]
        [1,2,1]
        [1,1,2]
      ]
    _.isEqual mine, expect
