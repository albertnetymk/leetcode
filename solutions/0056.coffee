require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

class Interval
  constructor: (@start, @end) ->

# merge :: [Interval] -> [Interval]
merge = (intervals) ->
  return [] if intervals.length is 0
  intervals.sort((i1, i2) -> i1.start - i2.start)
  result = [intervals[0]]
  for i in [1...intervals.length] by 1
    current = intervals[i]
    previous = result[result.length-1]
    if current.start <= previous.end
      previous.end = max previous.end, current.end
    else
      result.push current
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'merge', () ->
    cases =
      [
        [[[1,3],[2,6],[8,10],[15,18]], [[1,6],[8,10],[15,18]]]
        [[[1,4],[2,3]], [[1,4]]]
      ]
    for c in cases
      intervals = (new Interval e[0], e[1] for e in c[0])
      expect = c[1]
      actual = merge intervals
      actual = ([i.start, i.end] for i in actual)
      if not _.isEqual actual, expect
        p "expecting #{expect} but found #{actual}"
        return false
    true

  jsc.property 'merge/empty', () ->
    intervals = []
    expect = []
    actual = merge intervals
    actual = ([i.start, i.end] for i in actual)
    _.isEqual actual, expect
