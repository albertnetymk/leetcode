require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

lengthOfLongestSubstring = (str) ->
  result = 0
  set = {}
  start = 0
  for c,i in str
    if c of set
      result = Math.max result , i-start
      start = Math.max start, set[c] + 1
    set[c] = i
  return Math.max result , i-start

unique_str = jsc.array(jsc.asciichar).smap(_.uniq, _.identity)
unique_str_array = jsc.nearray unique_str

describe test_name(__filename), ->
  jsc.property 'unique string', unique_str, (str) ->
    l = lengthOfLongestSubstring str
    l is str.length

  jsc.property 'particular cases', () ->
    cases =
      'dvdf': 3
      'abba': 2
    for str,v of cases
      l = lengthOfLongestSubstring str
      if l isnt v
        return false
    true
