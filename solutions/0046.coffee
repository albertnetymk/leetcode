require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

remove = (arr, x) -> arr.filter (e) -> e isnt x

# permute :: [Integer] -> [[Integer]]
permute = (nums) ->
  return [[]] if nums.length is 0
  result = []
  for x in nums
    for rest in permute remove nums, x
      result.push [x].concat rest
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'permute', () ->
    mine = permute [1,2,3]
    expect =
      [
        [1,2,3]
        [1,3,2]
        [2,1,3]
        [2,3,1]
        [3,1,2]
        [3,2,1]
      ]
    _.isEqual mine, expect
