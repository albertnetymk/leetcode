require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# maxSubArray :: [Int] -> Int
maxSubArray = (nums) ->
  global_max_sum = max_sum_end_at = nums[0]
  for e in nums[1..]
    max_sum_end_at = max e, e+max_sum_end_at
    global_max_sum = max global_max_sum, max_sum_end_at
  global_max_sum

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'maxSubArray', () ->
    expect = 6
    nums = [-2,1,-3,4,-1,2,1,-5,4]
    actual = maxSubArray nums
    unless expect is actual
      p "expecting #{expect} but found #{actual}"
      return false
    true
