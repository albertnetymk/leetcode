require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

new_bool_array = (size) ->
  ret = new Array(size)
  for i in [0...size] by 1
    ret[i] = false
  ret

# totalNQueens :: Int -> Int
totalNQueens = (n) ->
  cols = new_bool_array n
  # diagnal / x - y = const
  d1 = new_bool_array (n*2)
  # diagnal \ x + y = const
  d2 = new_bool_array (n*2)

  count = 0

  dfs = (row) ->
    if row is n
      count++
      return
    for c in [0...n]
      d1_index = c - row + n
      d2_index = c + row
      continue if cols[c] or d1[d1_index] or d2[d2_index]
      cols[c] = d1[d1_index] = d2[d2_index] = true
      dfs row+1
      cols[c] = d1[d1_index] = d2[d2_index] = false

  dfs 0
  count

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'totalNQueens', () ->
    expect = 2
    actual = totalNQueens 4
    unless expect is actual
      p "expecting #{expect} but found #{actual}"
      return false
    true
