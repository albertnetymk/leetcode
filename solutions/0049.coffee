require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

normalize = (str) ->
  arr = str.split ''
  arr.sort((a, b) -> a.localeCompare b)
  arr.join ''

# groupAnagrams :: [String] -> [[String]]
groupAnagrams = (strs) ->
  dict = {}
  for str in strs
    normal = normalize str
    if dict[normal] is undefined
      dict[normal] = [str]
    else
      dict[normal].push str
  dict[k] for k in Object.keys dict

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'groupAnagrams', () ->
    strs = ["eat", "tea", "tan", "ate", "nat", "bat"]
    actual = groupAnagrams strs
    expect =
      [
        ["ate", "eat","tea"]
        ["nat","tan"]
        ["bat"]
      ]
    str.sort() for str in expect
    str.sort() for str in actual
    expect.sort()
    actual.sort()
    unless _.isEqual expect, actual
      p "expecting #{expect} but found #{actual}"
      return false
    true
