require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

char_eq = (c1, c2) -> c1 is c2 or c2 is '.'

# many :: String -> Char -> Integer
many = (str, target) ->
  for c,i in str
    if not char_eq c, target
      break
  i

match_recurse = (str, pattern_str) ->
  return str.length is 0 if pattern_str.length is 0
  if pattern_str.length > 1 and pattern_str[1] is '*'
    advance = many str, pattern_str[0]
    while advance >= 0
      return true if match_recurse str[advance..], pattern_str[2..]
      advance--
    return false
  str.length > 0 and
    char_eq(str[0], pattern_str[0]) and
    match_recurse str[1..], pattern_str[1..]

p_matrix = (matrix) ->
  for row in matrix
    for col in row
      col = if col then 1 else 0
      process.stdout.write col + ', '
    p ''

match_dynamic = (str, pattern_str) ->
  patterns = []
  for c,i in pattern_str
    if pattern_str[i..].length > 1 and pattern_str[i+1] is '*'
      continue
    if c is '*'
      patterns.push pattern_str[i-1..i]
      continue
    patterns.push c
  matrix = new Array(patterns.length + 1)

  for i in [0...matrix.length]
    matrix[i] = new Array(str.length+1)
    for j in [0...matrix[i].length]
      matrix[i][j] = false

  matrix[0][0] = true

  # first column
  for i in [0...patterns.length]
    matrix[i+1][0] = matrix[i][0] and patterns[i].length is 2

  for i in [0...patterns.length]
    for j in [0...str.length]
      # consume one token from each source
      diag = matrix[i][j] and char_eq str[j], patterns[i][0]
      # star matches more than once
      left = matrix[i+1][j] and patterns[i].length > 1 and char_eq str[j], patterns[i][0]
      # star matches empty string
      up = matrix[i][j+1] and patterns[i].length > 1
      matrix[i+1][j+1] = diag or left or up
  matrix[patterns.length][str.length]

isMatch = (str, pattern_str) ->
  match_dynamic str, pattern_str
  # match_recurse str, pattern_str

describe test_name(__filename), ->

  jsc.property 'isMatch', () ->
    cases = [
      ['bbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbbb', 'b*c*b*c*b*c*b*c*b*c', false]
      ['aade', 'a*c*e', false]
      ['', 'a*.', false]
      ['ac', '.*.*c*', true]
      ['ac', '.*.*c*c', true]
      ['bb', 'b*.c*b', true]
      ['', '', true]
      ['a', '', false]
      ['a', 'a.', false]
      ['', '.', false]
      ['a', '.', true]
      ['ab', '.', false]
      ['ab', '..', true]
      ['aaa', '...', true]
      ['a', 'a', true]
      ['aa', 'aa', true]
      ['aaa', 'aa', false]
      ['aa', 'a*aaa', false]
      ['aa', 'a*', true]
      ['aab', 'c*a*b', true]
      ['aaa', 'a*a', true]
      ['aaab', 'a*aaab', true]
      ['aaa', 'ab*a*c*a', true]
      ['aa', '.*b', false]
      ['aa', '.*bc', false]
      ['aa', '.*', true]
      ['ab', '.*', true]
      ['ab', '.*b', true]
      ['aaba', 'a*aba', true]
      ['aaba', 'a*.*aba', true]
      ['aaab', 'a*', false]
      ['aaab', 'a*bc', false]
      ['aaab', 'a*c', false]
      ['aaab', 'a*.*', true]
      ['aaabc', 'a*.*', true]
      ['aaa', 'a*.*a', true]
      ['aaca', 'ab*a*c*a', true]
      ['aacda', 'ab*a*c*a', false]
      ['ab', '.*..', true]
      ['aasdfasdfasdfasdfas', 'aasdf.*asdf.*asdf.*asdf.*s', true]
      ['aba', '.*.*', true]
      ['baabbbaccbccacacc', 'c*..b*a*a.*a..*c', true]
      ['ac', '.*.*c*.', true]
      ['ac', '.*.*c*..', true]
      ['ac', '.*.*c*.b', false]
      ['bbbaccbbbaababbac', '.b*b*.*...*.*c*.', true]
      ['bbcacbabbcbaaccabc', 'b*a*a*.c*bb*b*.*.*', true]
      ['baccbbcbcacacbbc', 'c*.*b*c*ba*b*b*.a*', true]
    ]
    for c in cases
      str = c[0]
      pattern = c[1]
      expect = c[2]
      actual = isMatch str, pattern
      unless expect is actual
        p str
        p pattern
        p "expecting #{expect} but found #{actual}"
        return false
    true

  jsc.property 'many', () ->
    cases = [
      ['', 'a', 0]
      ['a', 'a', 1]
      ['aa', 'a', 2]
      ['aab', 'a', 2]
      ['aaca', 'a', 2]
      ['aaca', '.', 4]
    ]
    for c in cases
      str = c[0]
      target = c[1]
      expect = c[2]
      actual = many str, target
      unless _.isEqual expect, actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
