require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

removeDuplicates = (nums) ->
  l = if nums.length > 0 then 1 else 0
  for n in nums
    if n > nums[l-1]
      nums[l++] = n
  l

naive_unique = (nums) ->
  uniques = _.uniq nums, true
  uniques.length

sort = (nums) ->
  _.sortBy nums, (x) -> x
add_extra = (nums) ->
  l = nums.length
  return nums if l is 0
  index = Math.floor(Math.random() * l)
  nums.push nums[index]
  nums
pop = (nums) ->
  return nums if nums.length is 0
  nums.pop()
  nums
sorted_array = jsc.array(jsc.integer).smap(add_extra, pop).smap(sort, _.identity)

describe test_name(__filename), ->
  jsc.property 'remove duplicate from array', sorted_array, (arr) ->
    l1 = removeDuplicates arr[..]
    l2 = naive_unique arr[..]
    l1 is l2
