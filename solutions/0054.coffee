require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# spiralOrder :: [[Int]] -> [Int]
spiralOrder = (matrix) ->
  result = []
  outer_layer = (row, col, row_n, col_n) ->
    return if row_n <= 0 or col_n <= 0
    # top row
    for j in [col...col+col_n] by 1
      result.push matrix[row][j]
    # rightmost col
    for i in [row+1...row+row_n] by 1
      result.push matrix[i][col+col_n-1]
    return if row_n is 1 or col_n is 1
    # bottom row
    for j in [col+col_n-2..col] by -1
      result.push matrix[row+row_n-1][j]
    # leftmost col
    for i in [row+row_n-2...row] by -1
      result.push matrix[i][col]

    outer_layer row+1, col+1, row_n-2, col_n-2
  outer_layer 0, 0, matrix.length, matrix[0]?.length
  result

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'spiralOrder/square', () ->
    expect = [1,2,3,6,9,8,7,4,5]
    matrix =
      [
        [ 1, 2, 3 ]
        [ 4, 5, 6 ]
        [ 7, 8, 9 ]
      ]

    actual = spiralOrder matrix
    _.isEqual actual, expect
  jsc.property 'spiralOrder/rectangle', () ->
    expect = [2,3]
    matrix =
      [
        [ 2, 3 ]
      ]

    actual = spiralOrder matrix
    _.isEqual actual, expect
  jsc.property 'spiralOrder/empty', () ->
    expect = []
    matrix = [ ]

    actual = spiralOrder matrix
    _.isEqual actual, expect
