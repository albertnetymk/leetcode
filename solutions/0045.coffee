require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# jump :: [Integer] -> Boolean
jump = (nums) ->
  level = 0
  i = border = 0
  target = nums.length-1

  while true
    next_border = border
    while i <= border
      return level if border >= target
      next_border = max next_border, i + nums[i]
      i++
    level++
    border = next_border
  # unreacheable
  -1

# I thought bfs would be enough...
naive_bfs = (nums) ->
  # outs :: Index -> [Index]
  outs = (index) -> i for i in [index+1..index+nums[index]] by 1

  visited = Array(nums.length).fill false

  step = 0
  frontier = [0]
  while true
    return step if frontier.indexOf(target) isnt -1
    new_frontier = []
    for i in frontier
      visited[i] = true
      new_frontier = new_frontier.concat outs i
    new_frontier = Array.from(new Set(new_frontier))
    frontier = (i for i in new_frontier when not visited[i])
    step++

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'jump', () ->
    cases = [
      [2, [2,3,1,1,4]]
    ]

    for c in cases
      expect = c[0]
      actual = jump c[1]
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        p c
        return false
    true
