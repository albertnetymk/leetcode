require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

String.prototype.empty = ->
  this.length is 0

MAX = 2147483647
MIN = -2147483648

overflow = (base, next) ->
  max = MAX
  # base * 10 + next > MAX
  base > Math.floor (max - next) / 10

parse_number = (str, sign) ->
  return 0 if str.empty()
  base = 0
  for char in str
    code = char.charCodeAt(0)
    min = '0'.charCodeAt(0)
    max = '9'.charCodeAt(0)
    switch
      when code < min or code > max
        return sign * base
      else
        next = code - min
        if overflow base, next
          return if sign is 1 then MAX else MIN
        base = base * 10 + next
  sign*base

myAtoi = (str) ->
  str = str.replace(/^ */, '')
  return 0 if str.empty()
  switch str[0]
    when '+'
      parse_number str[1..], 1
    when '-'
      parse_number str[1..], -1
    else
      parse_number str, 1

describe test_name(__filename), ->
  jsc.property 'myAtoi', () ->
    cases = [
      ['', 0]
      ['+-', 0]
      ['+1', 1]
      ['2147483647', 2147483647]
      ['-2147483648', -2147483648]
      ['2147483648', 2147483647]
      ['-2147483649', -2147483648]
      ['  010',  10]
      ['0 123', 0]
    ]
    for c in cases
      str = c[0]
      expect = c[1]
      actual = myAtoi str
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true

  jsc.property 'parse_number', () ->
    cases = [
      ['', 1, 0]
      ['+', 1, 0]
      ['-', 1, 0]
      ['1', 1, 1]
      ['1', -1, -1]
      ['+12', -1, 0]
      ['12', -1, -12]
      ['4a1', 1, 4]
    ]
    for c in cases
      str = c[0]
      sign = c[1]
      expect = c[2]
      actual = parse_number str, sign
      unless expect is actual
        p "expecting #{expect} but found #{actual}"
        return false
    true
