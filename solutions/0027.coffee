require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# removeElement ::
removeElement = (nums, val) ->
  for i in [nums.length-1..0] by -1
    if nums[i] is val
      nums.splice i, 1
  nums.length

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'removeElement', () ->
    nums = [3, 2, 2, 3]
    ans = 2
    my = removeElement nums, 3
    all [
      ans = my
      _.isEqual nums, [2,2]
    ]
