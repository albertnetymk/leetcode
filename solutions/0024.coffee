require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# swapPairs :: Node -> Node
swapPairs = (head) ->
  dummy = next: head
  pre = dummy
  while true
    fst = pre.next
    break if fst is null or fst.next is null

    snd = fst.next
    fst.next = snd.next
    snd.next = fst
    pre.next = snd

    pre = fst

  dummy.next
  

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true
  class Node
    constructor: (@val, @next) ->
  build_from_list = (list) ->
    arr = (new Node(e, null) for e in list)
    for i in [1..arr.length-1] by 1
      arr[i-1].next = arr[i]
    arr[0]

  jsc.property 'swapPairs', () ->
    l1 =  build_from_list [1..7]
    ans =  build_from_list [2,1,4,3,6,5,7]
    my = swapPairs l1
    _.isEqual ans, my
