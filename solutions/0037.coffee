require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

collect_used = (board, i, j) ->
  dict = {}
  for col in [0...9]
    if board[i][col] isnt '.'
      dict[board[i][col]] = true
  for row in [0...9]
    if board[row][j] isnt '.'
      dict[board[row][j]] = true
  base_row = Math.floor(i/3)*3
  base_col = Math.floor(j/3)*3
  for row in [0...3]
    for col in [0...3]
      if board[base_row+row][base_col+col] isnt '.'
        dict[board[base_row+row][base_col+col]] = true
  dict

# solve :: [[Char]] -> Bool
solve = (board) ->
  for i in [0...9]
    for j in [0...9]
      if board[i][j] is '.'
        used = collect_used board, i, j
        for v in [1..9] when not used[v]
          board[i][j] = v
          return true if solve board
        board[i][j] = '.'
        return false
  true

# solveSudoku :: [[Char]] -> void
solveSudoku = (board) ->
  solve board
  return

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'solveSudoku', () ->
    cases = [
      [
        [ [ '.', '.', '9', '7', '4', '8', '.', '.', '.' ],
          [ '7', '.', '.', '.', '.', '.', '.', '.', '.' ],
          [ '.', '2', '.', '1', '.', '9', '.', '.', '.' ],
          [ '.', '.', '7', '.', '.', '.', '2', '4', '.' ],
          [ '.', '6', '4', '.', '1', '.', '5', '9', '.' ],
          [ '.', '9', '8', '.', '.', '.', '3', '.', '.' ],
          [ '.', '.', '.', '8', '.', '3', '.', '2', '.' ],
          [ '.', '.', '.', '.', '.', '.', '.', '.', '6' ],
          [ '.', '.', '.', '2', '7', '5', '9', '.', '.' ] ]
        [ [ 5, 1, '9', '7', '4', '8', 6, 3, 2 ],
          [ '7', 8, 3, 6, 5, 2, 4, 1, 9 ],
          [ 4, '2', 6, '1', 3, '9', 8, 7, 5 ],
          [ 3, 5, '7', 9, 8, 6, '2', '4', 1 ],
          [ 2, '6', '4', 3, '1', 7, '5', '9', 8 ],
          [ 1, '9', '8', 5, 2, 4, '3', 6, 7 ],
          [ 9, 7, 5, '8', 6, '3', 1, '2', 4 ],
          [ 8, 3, 2, 4, 9, 1, 7, 5, '6' ],
          [ 6, 4, 1, '2', '7', '5', '9', 8, 3 ] ]
      ]
    ]
    for c in cases
      solveSudoku c[0]
      actual = c[0]
      expect = c[1]
      unless _.isEqual expect, actual
        p "expecting"
        p expect
        p "but found"
        p actual
        return false
    true
