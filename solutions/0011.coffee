require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

max = (x, y) ->
  if x > y then x else y

# maxArea :: [Int] -> Int
maxArea = (heights) ->
  [ret, i, j] = [0, 0, heights.length-1]
  while i < j
    h = if heights[i] < heights[j] then heights[i++] else heights[j--]
    ret = max ret, (1+j-i) * h
  ret

describe test_name(__filename), ->
  jsc.property 'maxArea', () ->
    maxArea([1,2,1]) is 2
