require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

last_larger_than_index = (arr, l, r, x) ->
  while r >= l
    if arr[r] > x
      return r
    r--
  p "error"

swap = (arr, i, j) ->
  tmp = arr[i]
  arr[i] = arr[j]
  arr[j] = tmp

reverse = (arr, i, j) ->
  while i < j
    swap arr, i++, j--

nextPermutation = (nums) ->
  l = nums.length
  return if l <= 1
  i = l-2
  while i>=0
    if nums[i] < nums[i+1]
      value = nums[i]
      j = last_larger_than_index nums, i+1, l-1, value
      swap nums, i, j
      break
    i--
  reverse nums, i+1, l-1
  return

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  cmp_array = (a1, a2) ->
    unless _.isEqual a1, a2
      p "expecting #{a2} but found #{a1}"
      return false
    true

  jsc.property 'nextPermutation', () ->
    cases = [
      [[1,2,3], [1,3,2]]
      [[3,2,1], [1,2,3]]
      [[1,1,5], [1,5,1]]
    ]

    for c in cases
      nums = c[0]
      nextPermutation nums
      unless cmp_array nums, c[1]
        return false
    true
