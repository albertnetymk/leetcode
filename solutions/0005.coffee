require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

reflect = (str, middle, length) ->
  return str if length <= 0
  rest = str.substring middle+1
  small_half_length = (length-1) >> 1
  head = str.substring 0, middle-small_half_length
  first = str.substring middle-small_half_length, middle
  second = reverse first
  if length % 2 is 0
    center = [str[middle], str[middle]].join ''
  else
    center = str[middle]

  head.concat first, center, second, rest

diff = (actual, expect) ->
  p expect.length
  p actual.length
  p "expecting #{expect} but found #{actual}"

reverse = (str) ->
  str.split('').reverse().join ''

is_palindrome = (str) ->
  reversed = reverse str
  str is reversed

longestPalindrome = (str) ->
  length = str.length
  return str if length <= 1
  longest =
    end: 1
    length: 1
  for c,i in str
    for j in [0..1]
      left = i - 1
      right = i + j
      new_length = j
      while left >= 0 and right < length and str[left] is str[right]
        left--
        right++
        new_length += 2
      if new_length > longest.length
        longest.length = new_length
        longest.end = right
  str.substring longest.end-longest.length, longest.end

unique_array = jsc.nearray(jsc.asciichar).smap(_.uniq, _.identity)
rand = (upper) ->
  Math.floor(Math.random() * upper)

describe test_name(__filename), ->
  jsc.property 'single palindrome', unique_array, (arr) ->
    str = arr.join ''
    middle = rand str.length
    length = rand(middle) + 1
    return true if length is 1
    new_str = reflect str, middle, length
    ret = longestPalindrome new_str
    if not ((is_palindrome ret) and (ret.length is length))
      diff ret, new_str
      return false
    true

  jsc.property 'nested palindrome', unique_array, (arr) ->
    str = arr.join ''
    new_str = reflect str, str.length-1, 2*str.length
    new_str = 'a'.concat new_str, 'a'
    ret = longestPalindrome new_str
    if not ((is_palindrome ret) and (ret.length is new_str.length))
      diff ret, new_str
      return false
    true

  jsc.property 'special cases', () ->
    cases = [
      ['aba', 'aba']
      ['abac', 'aba']
      ['cabac', 'cabac']
      ['caba', 'aba']
      ['caa', 'aa']
      ['abbaa', 'abba']
      ['bananas', 'anana']
      ['abaaba', 'abaaba']
      ['aabbaabbaa', 'aabbaabbaa']
    ]
    for c in cases
      str = c[0]
      expect = c[1]
      actual = longestPalindrome str
      if actual isnt expect
        diff actual, expect
        return false
    true

  jsc.property 'is palindrome', () ->
    cases =
      'a': 1
      'aba': 1
      'abc': 0
    for k,v of cases
      expect = if v is 1 then true else false
      actual = is_palindrome k
      return false if actual isnt expect
    true

  jsc.property 'reflect', () ->
    cases = [
      ['a', 0, 1, 'a']
      ['ab', 0, 1, 'ab']
      ['ab', 1, 2, 'abb']
      ['abc', 1, 2, 'abbc']
    ]
    for c in cases
      str = c[0]
      middle = c[1]
      length = c[2]
      expect = c[3]
      actual = reflect str, middle, length
      if actual isnt expect
        diff actual, expect
        return false
    true
