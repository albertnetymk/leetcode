require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

sort = (arr) -> arr.sort((x,y) -> x - y)

abs = Math.abs

# threeSumClosest :: [Int] -> Int -> Int
threeSumClosest = (nums, target) ->
  sort nums
  len = nums.length
  diff = Number.MAX_SAFE_INTEGER
  minsum = 0
  for n in [0..len-1] by 1
    i = n+1
    j = len-1
    while i < j
      sum = nums[n] + nums[i] + nums[j]
      diff_c = abs(sum - target)
      if diff > diff_c
        diff = diff_c
        minsum = sum
      if sum < target
        i++
      else if sum > target
        j--
      else
        return minsum
  minsum

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'threeSumClosest', () ->
    all [
      2 == threeSumClosest [-1, 2, 1, -4], 1
      3 == threeSumClosest [0, 1, 2], 0
    ]
