require('mocha-inline')()
require '../util/test_name'
jsc = require('jsverify')
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# isMatch :: String -> String -> Boolean
isMatch = (str, pattern) ->
  m = str.length
  n = pattern.length

  dp = Array(m+1).fill(0).map(() -> Array(n+1).fill(false))

  dp[0][0] = true

  for j in [0..n-1] by 1
    dp[0][j+1] = dp[0][j] && pattern[j] is '*'

  for i in [1..m] by 1
    for j in [1..n] by 1
      diag = dp[i-1][j-1] &&
              ((str[i-1] is pattern[j-1]) or (pattern[j-1] is '?'))
      up = dp[i-1][j] && (pattern[j-1] is '*')
      left = dp[i][j-1] && (pattern[j-1] is '*')

      dp[i][j] = diag or up or left

  dp[m][n]

describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'isMatch', () ->
    cases =  [
      [['',''], true]
      [['aa','a'], false]
      [['aa','aa'], true]
      [['aaa','aa'], false]
      [['aa', '*'], true]
      [['aa', 'a*'], true]
      [['ab', '?*'], true]
      [['aab', 'c*a*b'], false]
      [['ab*cdec', 'ab*c'], true]
    ]

    for c in cases
      actual = isMatch c[0][0], c[0][1]
      expect = c[1]
      unless expect is actual
        p c
        p "expecting #{expect} but found #{actual}"
        return false
    true
