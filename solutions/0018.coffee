require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

# fourSum :: [Int] -> Int -> [[Int]]
fourSum = (nums, target) ->
  sort nums
  len = nums.length
  ret = {}
  for i in [0..len-1] by 1
    for j in [i+1..len-1] by 1
      k = j+1
      l = len-1
      while k < l
        arr = [a, b, c, d] = [nums[i], nums[j], nums[k], nums[l]]
        sum = a + b + c + d
        if sum < target
          k++
        else if sum > target
          l--
        else
          ret[arr] = arr
          k++
  v for k,v of ret

describe test_name(__filename), ->
  eq = (arr1, arr2) ->
    _.isEqual arr1.sort, arr2.sort

  jsc.property 'fourSum', () ->
    ans = [
      [-1,  0, 0, 1]
      [-2, -1, 1, 2]
      [-2,  0, 0, 2]
    ]
    my = fourSum [1, 0, -1, 0, -2, 2], 0
    eq ans, my
