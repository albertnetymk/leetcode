require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log
max = Math.max
min = Math.min
abs = Math.abs
sort = (arr) -> arr.sort((x,y) -> x - y)

dict =
  2: 'abc'
  3: 'def'
  4: 'ghi'
  5: 'jkl'
  6: 'mno'
  7: 'pqrs'
  8: 'tuv'
  9: 'wxyz'

# letterCombinations :: [Char] -> [String]
letterCombinations = (digits) ->
  return [] if digits.length is 0
  helper = (digits) ->
    return [""] if digits.length is 0
    [d, rest...] = digits
    ret = []
    for c in dict[d]
      for r in helper rest
        ret.push c + r
    ret

  helper(digits)


describe test_name(__filename), ->
  all = (arr) ->
    for e in arr
      return false if not e
    true

  jsc.property 'letterCombinations', () ->
    _.isEqual ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"],
      letterCombinations('23')
