require('mocha-inline')()
require '../util/test_name'
jsc = require("jsverify")
_ = require 'underscore'

p = console.log

# longestCommonPrefix :: [String] -> String
longestCommonPrefix = (strs) ->
  return "" if strs.length is 0
  prefix = ""
  last = 0
  while true
    str = strs[0]
    if last >= str.length
      return prefix
    c = str[last]
    for str in strs[1..]
      if last >= str.length || c isnt str[last]
        return prefix
    prefix = prefix + c
    last++

describe test_name(__filename), ->
  jsc.property 'longestCommonPrefix', () ->
    'ab' == longestCommonPrefix(['ab', 'ab', 'abc'])
