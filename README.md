<!-- This file is generated from `doc` folder. -->

My LeetCode page: https://leetcode.com/albertnetymk/

Try to code up for problems in [LeetCode](https://leetcode.com/problemset/algorithms/).

The solution in this repo is implemented using [CoffeeScript](http://coffeescript.org), and [jsverify](https://github.com/jsverify/jsverify) is used
for property based testing. The `others` column contains solutions I steal from or I find neat.


ID     | Name | Solution | Others
---- : | ---- | ----     | ----
1 | Two Sum | [CoffeeScript](./solutions/0001.coffee) | [No comment.](https://leetcode.com/discuss/48602/11-lines-and-1-for-in-java)
2 | Add Two Numbers | [CoffeeScript](./solutions/0002.coffee) | [It's quite clever to use one extra node in the beginning, and discard it on return. My original implementation is using recursion.](https://leetcode.com/discuss/51471/python-concise-solution)
3 | Longest Substring Without Repeating Characters | [CoffeeScript](./solutions/0003.coffee) | -
4 | Median of Two Sorted Arrays | [CoffeeScript](./solutions/0004.coffee) | [By treating positions as elements, it unifies the treatment of odd/even cases.](https://leetcode.com/discuss/41621/very-concise-iterative-solution-with-detailed-explanation)
5 | Longest Palindromic Substring | [CoffeeScript](./solutions/0005.coffee) | [Since palindrome is a symmetric property, it's more straightforward to begin the searching from the middle.](https://leetcode.com/discuss/37658/share-my-concise-javascript-solution)
6 | ZigZag Conversion | [CoffeeScript](./solutions/0006.coffee) | [The solution becomes much simpler after using string length as the terminating condition. I feel my way of using map is less error-prone compared with direct array accessing.](https://leetcode.com/discuss/51476/lines-space-accepted-solution-with-detailed-explantation)
7 | Reverse Integer | [CoffeeScript](./solutions/0007.coffee) | [Integer division feels so natural in C.](https://leetcode.com/discuss/46288/my-4ms-c-solution)
8 | String to Integer (atoi) | [CoffeeScript](./solutions/0008.coffee) | [It didn't occur to me that the overflowing check for positive and negative numbers could be combined; since we are dealing with integers, only checking the positive overflowing is enough.](https://leetcode.com/discuss/54778/java-simple-clean-and-fast)
9 | Palindrome Number | [CoffeeScript](./solutions/0009.coffee) | [It's quite smart to construct the reversed number on the fly and compare if they are the same. The most ingenious part lies in the handling of the most significant bit to avoid overflowing. Actually, the loop could stop in the middle of the number, but calculating the total number of digits is not so straightforward, so the solution is rather neat.](https://leetcode.com/discuss/51643/6-line-c%23-solution)
10 | Regular Expression Matching | [CoffeeScript](./solutions/0010.coffee) | [I didn't find any particular solution I like, but I didn't think about dynamic programing, until I read the threads in Discussion. I feel my solutions, both depth-first search and dynamic programming, are much readable. (Of course, I am biased.)](https://leetcode.com/discuss/questions/oj/regular-expression-matching)
11 | Container With Most Water | [CoffeeScript](./solutions/0011.coffee) | [Very concise implementation.](https://discuss.leetcode.com/topic/45725/4-lines-java-code)
12 | Integer to Roman | [CoffeeScript](./solutions/0012.coffee) | [It's mind-blowing to see such elegant solution. The gist is to specify [0..9] for base [1, 10, 100, 1000].](https://discuss.leetcode.com/topic/12384/simple-solution)
13 | Roman to Integer | [CoffeeScript](./solutions/0013.coffee) | [Basically a reprint](https://discuss.leetcode.com/topic/17110/my-straightforward-python-solution/2)
14 | Longest Common Prefix | [CoffeeScript](./solutions/0014.coffee) | [No particular implementations I am fond of, but this problem is so simple anyway.](https://discuss.leetcode.com/category/22/longest-common-prefix)
15 | 3Sum | [CoffeeScript](./solutions/0015.coffee) | [Others' solutions seem fairly complicated.](https://discuss.leetcode.com/category/23/3sum)
16 | 3Sum Closest | [CoffeeScript](./solutions/0016.coffee) | [Very clean, so copied shamelessly.](https://discuss.leetcode.com/topic/42459/a-12ms-easy-understanding-solution)
17 | Letter Combinations of a Phone Number | [CoffeeScript](./solutions/0017.coffee) | [It's fairly short, but I feel the recursive version is more approachable.](https://discuss.leetcode.com/topic/46309/concise-python-solution)
18 | 4Sum | [CoffeeScript](./solutions/0018.coffee) | [Very similar to 3sum.](https://discuss.leetcode.com/category/26/4sum)
19 | Remove Nth Node From End of List | [CoffeeScript](./solutions/0019.coffee) | [The maintaining-gap trick is very clever.](https://leetcode.com/articles/remove-nth-node-end-list/)
20 | Valid Parentheses | [CoffeeScript](./solutions/0020.coffee) | -
21 | Merge Two Sorted Lists | [CoffeeScript](./solutions/0021.coffee) | -
22 | Generate Parentheses | [CoffeeScript](./solutions/0022.coffee) | [I know I shall solve this problem recursively, but I don't know how to decompose it recursively.](https://discuss.leetcode.com/topic/52138/one-line-python-solution)
23 | Merge k Sorted Lists | [CoffeeScript](./solutions/0023.coffee) | [It's a pity that JS doesn't have heap in its standard library.](https://discuss.leetcode.com/topic/54252/simple-python-solution)
24 | Swap Nodes in Pairs | [CoffeeScript](./solutions/0024.coffee) | -
25 | Reverse Nodes in k-Group | [CoffeeScript](./solutions/0025.coffee) | [I was using a k-size array, but apparently one could get it to work with one iteration.](https://discuss.leetcode.com/topic/54589/share-my-java-recursive-solution)
26 | Remove Duplicates from Sorted Array | [CoffeeScript](./solutions/0026.coffee) | [It's quite concise after removing some unnecessary variables.](https://leetcode.com/discuss/42500/5-lines-c-java-better-loops)
27 | Remove Element | [CoffeeScript](./solutions/0027.coffee) | [No comment.](https://discuss.leetcode.com/topic/54342/accepted-solution-of-javascript-just-need-4-lines)
28 | Implement strStr() | [CoffeeScript](./solutions/0028.coffee) | -
29 | Divide Two Integers | [CoffeeScript](./solutions/0029.coffee) | [My version uses the same idea, but needs to cope with auto type conversion and lack of 64bit shifting support in JS. Not sure why it's almost the slowest among all JS submissions...](https://discuss.leetcode.com/topic/15568/detailed-explained-8ms-c-solution)
30 | Substring with Concatenation of All Words | [CoffeeScript](./solutions/0030.coffee) | [The sliding window technique seems fairly neat. I recall using sth similar in the container problem.](https://discuss.leetcode.com/topic/6617/an-o-n-solution-with-detailed-explanation)
31 | Next Permutation | [CoffeeScript](./solutions/0031.coffee) | [I came up with the exact same approach, haha.](https://leetcode.com/articles/next-permutation/)
32 | Longest Valid Parentheses | [CoffeeScript](./solutions/0032.coffee) | [It's quite neat to push unmatched ')' into the stack and use that as the new starting to calculate the length.](https://discuss.leetcode.com/topic/2289/my-o-n-solution-using-a-stack)
33 | Search in Rotated Sorted Array | [CoffeeScript](./solutions/0033.coffee) | [After finding the index of minimal value, I can decide which part I should do binary search instead of performing binary search twice.](https://discuss.leetcode.com/topic/3538/concise-o-log-n-binary-search-solution)
34 | Search for a Range | [CoffeeScript](./solutions/0034.coffee) | [Making two iterating fingers seems to simply the logic.](https://discuss.leetcode.com/topic/5891/clean-iterative-solution-with-two-binary-searches-with-explanation)
35 | Search Insert Position | [CoffeeScript](./solutions/0035.coffee) | [I was using recursion; not treating out of range specially makes the code much neat. Lessons learned.](https://discuss.leetcode.com/topic/7874/my-8-line-java-solution/2)
36 | Valid Sudoku | [CoffeeScript](./solutions/0036.coffee) | [The use of array instead of set when keys are known integers is ingenious. The C++ version is surely more concise. Darn; multiple dimension array in JS is so awkward to work with.](https://discuss.leetcode.com/topic/8241/my-short-solution-by-c-o-n2)
37 | Sudoku Solver | [CoffeeScript](./solutions/0037.coffee) | -
38 | Count and Say | [CoffeeScript](./solutions/0038.coffee) | [It seems misleading to others. I guess I had some presumption on reading the description.](https://discuss.leetcode.com/topic/1296/please-change-the-misleading-description)
39 | Combination Sum | [CoffeeScript](./solutions/0039.coffee) | [It's basically the same except mine is functional. I tried the imperative version as well, and the performance is on par.](https://discuss.leetcode.com/topic/14654/accepted-16ms-c-solution-use-backtracking-easy-understand)
40 | Combination Sum II | [CoffeeScript](./solutions/0040.coffee) | [My first reaction is to use dictionary to get rid of duplicates. The alternative approach is more intelligent. However, both seem quite slow (38% vs 41%).](https://discuss.leetcode.com/topic/19845/java-solution-using-dfs-easy-understand)
41 | First Missing Positive | [CoffeeScript](./solutions/0041.coffee) | [I was thinking about sorting the array firstly, but that's O(nlgn) already. The linked solution is truly concise; the idea of using array as a set when keys are all positive is extremely incisive.](https://discuss.leetcode.com/topic/8293/my-short-c-solution-o-1-space-and-o-n-time/2)
42 | Trapping Rain Water | [CoffeeScript](./solutions/0042.coffee) | [I was summing up level by level for each vat; quite tricky to get it to be correct. The alternative solution is super smart; it's similar to the solution to "11 Container With Most Water", which came across my mind on reading the problem description, but I didn't see how I can build on top it. In fact using single `altitude` simplifies the algorithm/code a bit.](https://discuss.leetcode.com/topic/5125/sharing-my-simple-c-code-o-n-time-o-1-space)
43 | Multiply Strings | [CoffeeScript](./solutions/0043.coffee) | [The linked solution is so elegant that I have to copy. (Mine was calculating each row and sum them up.)](https://discuss.leetcode.com/topic/30508/easiest-java-solution-with-graph-explanation)
44 | Wildcard Matching | [CoffeeScript](./solutions/0044.coffee) | [The clear use of `NULL` as the end of the string in C++ made the code so succinct. My dynamic programming approach is rather mundane.](https://discuss.leetcode.com/topic/3040/linear-runtime-and-constant-space-solution)
45 | Jump Game II | [CoffeeScript](./solutions/0045.coffee) | [I was using a shadow array keep track of levels for current index, but apparently, it's needed.](https://discuss.leetcode.com/topic/3191/o-n-bfs-solution)
46 | Permutations | [CoffeeScript](./solutions/0046.coffee) | [I actually like the succinct Haskell syntax for list comprehension; maybe list and array should be both supported in the same language.](https://shivindap.wordpress.com/2009/01/12/permutations-of-a-list-in-haskell/)
47 | Permutations II | [CoffeeScript](./solutions/0047.coffee) | [The idea of building the result iteratively is quite insightful.](https://leetcode.com/problems/permutations-ii/discuss/18602/)
48 | Rotate Image | [CoffeeScript](./solutions/0048.coffee) | [It's much easier than mine...](https://leetcode.com/problems/rotate-image/discuss/18872/)
49 | Group Anagrams | [CoffeeScript](./solutions/0049.coffee) | [The way of normalizing strings using frequency count is neat.](https://leetcode.com/problems/group-anagrams/solution/)
50 | Pow(x, n) | [CoffeeScript](./solutions/0050.coffee) | [After seeing the solution, I realized that I misapprehend the intention of this problem.](https://leetcode.com/problems/powx-n/solution/)
51 | N-Queens | [CoffeeScript](./solutions/0051.coffee) | -
52 | N-Queens II | [CoffeeScript](./solutions/0052.coffee) | [Using three boolean array for validation is pretty neat. In addition, DFS does feel more natural in a non-pure language. BFS fits better in a pure language.](https://leetcode.com/problems/n-queens-ii/discuss/20048/)
53 | Maximum Subarray | [CoffeeScript](./solutions/0053.coffee) | [I was clueless on trying to solve it, so I resorted to wikipedia. Apparently, this is a classical algorithmic problem. The idea of partitioning solutions into disjointed groups, sub-arrays ending in each index, is ingenious.](https://www.wikiwand.com/en/Maximum_subarray_problem)
54 | Spiral Matrix | [CoffeeScript](./solutions/0054.coffee) | -
55 | Jump Game | [CoffeeScript](./solutions/0055.coffee) | [I came up with that idea on my own, lol. Since we are not interested in *how* we reach the target, but only *if* we can reach the target, BFS is indeed an overkill. The new algorithm maintains a range that's reachable, and tries to expand it on each iteration.](https://discuss.leetcode.com/topic/4911/linear-and-simple-solution-in-c)
56 | Merge Intervals | [CoffeeScript](./solutions/0056.coffee) | -
57 | Insert Interval | [CoffeeScript](./solutions/0057.coffee) | [I used binary search; however, the first solution presented here is really succinct. Impressive.](https://leetcode.com/problems/insert-interval/discuss/21622)
575 | Distribute Candies | [CoffeeScript](./solutions/0575.coffee) | [Somehow I thought I need to keep track of the total number of each kind. Other than that, it's the same.](https://leetcode.com/problems/distribute-candies/solution/#approach-4-using-set-accepted)